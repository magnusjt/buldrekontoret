[IP] = the remote server ip

###############
## On the PI ##
###############

# fix keyboard layout
sudo nano /etc/default/keyboard
# Set XBLAYOUT="no"
# May need reboot (reboot now)

# change password for user pi
# Login as pi, then run:
passwd

# enable ssh
systemctl start ssh
systemctl enable ssh

# pro tip
Connect the raspberry pi to the local network.
Use regular computer to SSH/SFTP into the PI.
Now you can paste things from here instead of manually typing it in.
You can also copy ssh keys to the remote server more easily.

# install node js
curl -sL https://deb.nodesource.com/setup_7.x | sudo -E bash -
sudo apt install nodejs

# remote tunnel / autossh

# Install autossh
sudo apt-get install autossh

# Add autossh user
sudo adduser --system --group --disabled-password autossh

# Enable shell for autossh temporarily
sudo chsh --shell /bin/bash autossh

# Create a keyfile and add server to known hosts
sudo su autossh
cd ~
mkdir .ssh
chmod 700 .ssh

# add key to known hosts (could also just do a quick login attempt...)
ssh-keyscan [IP] >> ~/.ssh/known_hosts

# generate the keyfiles
ssh-keygen -t rsa

# copy key over to remote server (now would be a good time to be connected via ssh on local network)
cat ~/.ssh/id_rsa.pub
#exit

# disable autossh shell again
sudo chsh --shell /bin/false autossh

# copy the systemd unit file (make sure to update the server ip)
sudo cp autossh.service /lib/systemd/system/autossh.service
sudo cp autotruportal.service /lib/systemd/system/autotruportal.service

# create a symlink to the unit files
sudo ln -s /lib/systemd/system/autossh.service /etc/systemd/system/autossh.service
sudo ln -s /lib/systemd/system/autotruportal.service /etc/systemd/system/autotruportal.service

# start and enable autossh using the unit file
sudo systemctl daemon-reload
sudo systemctl start autossh
sudo systemctl start autotruportal
sudo systemctl enable autossh
sudo systemctl enable autotruportal

# Check status
systemctl status autossh
systemctl status autotruportal

###############
## On REMOTE ##
###############

# add the autossh system user
sudo adduser --system --group --shell /bin/false --disabled-password autossh
sudo mkdir -p /home/autossh/.ssh
sudo touch /home/autossh/.ssh/authorized_keys
sudo chmod 600 /home/autossh/.ssh/authorized_keys

sudo chown -R autossh:autossh /home/autossh/.ssh
sudo chmod 700 /home/autossh/.ssh