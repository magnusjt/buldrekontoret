# https://www.digitalocean.com/community/tutorials/how-to-secure-haproxy-with-let-s-encrypt-on-ubuntu-14-04
# https://certbot.eff.org/lets-encrypt/ubuntutrusty-haproxy

# Install haproxy
sudo add-apt-repository ppa:vbernat/haproxy-1.5
sudo apt-get update
sudo apt-get install haproxy
sudo nano /etc/default/haproxy # ENABLED=1
sudo service haproxy
Usage: /etc/init.d/haproxy {start|stop|reload|restart|status}
sudo cp /etc/haproxy/haproxy.cfg /etc/haproxy/haproxy.bak.cfg
# Copy in haproxy.cfg from this project

# Allow https port
sudo ufw allow 443

# Restart syslog to get haproxy logs
sudo service rsyslog restart

# Install certbot
sudo apt-get update
sudo apt-get install software-properties-common
sudo add-apt-repository ppa:certbot/certbot
sudo apt-get update
sudo apt-get install certbot

# Grab cert with certbot
sudo supervisorctl stop doorman
sudo certbot certonly --standalone --preferred-challenges http --http-01-port 80 -d app.buldrekontoret.com
# => /etc/letsencrypt/live/app.buldrekontoret.com/fullchain.pem
# => /etc/letsencrypt/live/app.buldrekontoret.com/privkey.pem
sudo supervisorctl start doorman

# Installer cert
sudo mkdir -p /etc/haproxy/certs
DOMAIN='app.buldrekontoret.com' sudo -E bash -c 'cat /etc/letsencrypt/live/$DOMAIN/fullchain.pem /etc/letsencrypt/live/$DOMAIN/privkey.pem > /etc/haproxy/certs/$DOMAIN.pem'
sudo chmod -R go-rwx /etc/haproxy/certs

# Change .env for doorman app to make the app run on port 3000 instead of 80 (NB: We don't need authbind anymore)
sudo supervisorctl restart doorman
sudo service haproxy restart

# Create renewal script
sudo nano /usr/local/bin/renew.sh # Copy from this project
sudo chmod u+x /usr/local/bin/renew.sh
sudo /usr/local/bin/renew.sh

# Update certbot conf
sudo nano /etc/letsencrypt/renewal/app.buldrekontoret.com.conf
# => http01_port = 54321
sudo certbot renew --dry-run

# Add cron job
sudo crontab -e
# => 30 2 * * * /usr/bin/certbot renew --deploy-hook "/usr/local/bin/renew.sh" >> /var/log/le-renewal.log

# Note! Remember to remove the default cron job in /etc/cron.d