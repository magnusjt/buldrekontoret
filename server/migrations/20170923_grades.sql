SET FOREIGN_KEY_CHECKS=0;

DELETE FROM routeColors;
DELETE FROM grades;

INSERT INTO routeColors(id, name, color, description, sortIndex) VALUES
(1, 'Hvit', 'white', 'Nybegynner', 10),
(2, 'Grønn', 'green', 'Lett', 20),
(3, 'Blå', 'blue', 'Litt mindre lett', 30),
(4, 'Gul', 'yellow', 'Klatra en stund', 40),
(5, 'Rød', 'red', 'Nesten hardt', 50),
(6, 'Svart', 'black', 'Hardt', 60),
(7, 'Sølv', 'silver', 'Hardt #fåsds', 70);

INSERT INTO grades(id, name, colorId, sortIndex) VALUES
(1, '3',   1, 0),
(2, '4',   1, 10),
(3, '5',   2, 20),
(4, '5+',  2, 30),
(5, '6A',  3, 40),
(6, '6A+', 3, 50),
(7, '6B',  4, 60),
(8, '6B+', 4, 70),
(9, '6C',  5, 80),
(10, '6C+', 5, 90),
(11, '7A',  6, 100),
(12, '7A+', 6, 110),
(13, '7B',  6, 120),
(14, '7B+', 6, 130),
(15, '7C',  7, 140),
(16, '7C+', 7, 150),
(17, '8A',  7, 160),
(18, '8A+', 7, 170),
(19, '8B',  7, 180),
(20, '8B+', 7, 190),
(21, '8C',  7, 200);

SET FOREIGN_KEY_CHECKS=1;