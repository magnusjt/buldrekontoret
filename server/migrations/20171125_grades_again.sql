UPDATE routeColors
  SET description='Nådeløst', sortIndex=70
  WHERE color='white';

UPDATE routeColors
  SET sortIndex=80, description='Umulig'
  WHERE color='silver';

UPDATE grades
  SET colorId=(SELECT id FROM routeColors WHERE color='white')
  WHERE name IN ('7B','7B+');

UPDATE grades
  SET colorId=(SELECT id FROM routeColors WHERE color='green')
  WHERE name IN ('3','4');