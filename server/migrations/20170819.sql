INSERT INTO routeColors(id, name, color, description, sortIndex) VALUES
(1, 'Grønn', 'green', 'Nybegynner', 10),
(2, 'Blå', 'blue', 'Lett', 20),
(3, 'Gul', 'yellow', 'Ikke så lett', 30),
(4, 'Rød', 'red', 'Nesten hardt', 40),
(5, 'Svart', 'black', 'Hardt', 50),
(6, 'Hvit', 'white', 'Hardt #fåsds', 60);

INSERT INTO grades(id, name, colorId, sortIndex) VALUES
(1, '3',   1, 0),
(2, '4',   1, 10),
(3, '5',   2, 20),
(4, '5+',  2, 30),
(5, '6A',  3, 40),
(6, '6A+', 3, 50),
(7, '6B',  3, 60),
(8, '6B+', 3, 70),
(9, '6C',  4, 80),
(10, '6C+', 4, 90),
(11, '7A',  5, 100),
(12, '7A+', 5, 110),
(13, '7B',  6, 120),
(14, '7B+', 6, 130),
(15, '7C',  6, 140),
(16, '7C+', 6, 150),
(17, '8A',  6, 160),
(18, '8A+', 6, 170),
(19, '8B',  6, 180),
(20, '8B+', 6, 190),
(21, '8C',  6, 200);

INSERT INTO cups(id, name, startsAt, endsAt, isCurrent) VALUES
(1, '2017/2018', '2017-09-01', '2018-04-01', 1);