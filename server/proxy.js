const path = require('path');
const env = require('getenv');
const express = require('express');
const proxy = require('express-http-proxy');
const request = require('request');
const middleware = require('./src/middleware');
const UserRepo = require('./src/repos/UserRepo');
const connectToDatabase = require('./src/util/connectToDatabase');

require('dotenv').config({
    silent: true,
    path: path.resolve(__dirname, './.env')
});

process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

const config = {
    PROXY_PORT: env.int('PROXY_PORT', 3005),
    PROXY_HOST: env.string('PROXY_HOST', '127.0.0.1:8080'),
    DB: {
        host:     env.string('DB_HOST', '127.0.0.1'),
        port:     env.string('DB_PORT', '3306'),
        user:     env.string('DB_USER', ''),
        password: env.string('DB_PASSWORD', ''),
        database: env.string('DB_NAME', 'doorman'),
    },
    SESSION_ENCRYPTION_KEY: env.string('SESSION_ENCRYPTION_KEY')
};

process.on('uncaughtException', err => {
    console.error(err);
    process.exit(1);
});

process.on('unhandledRejection', err => {
    console.error(err);
});

let db = connectToDatabase(config.DB);
let userRepo = new UserRepo(db);
let app = express();

app.use(middleware.session(db, config.SESSION_ENCRYPTION_KEY));
app.use(middleware.requireLoggedIn(userRepo));
app.use(function(req, res, next){
    if(!req.user.isAdmin){
        let err = new Error('Not authorized');
        err.status = 403;
        next(err);
    }

    next();
});
app.use(proxy(config.PROXY_HOST, {https: true, timeout: 60000}));
app.use(middleware.handleError());

app.listen(config.PROXY_PORT, () => {
    console.log('App listening on port ' + config.PROXY_PORT);
});