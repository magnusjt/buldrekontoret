const moment = require('moment');

class CheckIn{
    constructor(entity, coolDownHours){
        this._coolDownHours = coolDownHours;

        this.id = entity.id;
        this.userId =  entity.userId;
        this.checkedIn = entity.checkedIn;
        this.displayName = entity.firstname + ' ' + entity.lastname;
        this.expiresAt = moment(this.checkedIn).add(this._coolDownHours, "h");
    }

    cooledDown(){
        return moment().isAfter(this.expiresAt);
    }
}

class CheckInFactory{
    constructor(coolDownHours = 3){
        this._coolDownHours = coolDownHours;
    }

    create(entity){
        return new CheckIn(entity, this._coolDownHours);
    }
}

module.exports = {CheckIn, CheckInFactory};
