const {CheckIn} = require('./CheckIn')
const moment = require('moment')

describe('CheckIn', () => {
    let entity = {
        id: 0,
        userId: 0,
        checkedIn: moment().add(-1, 'h'),
        firstname: "foo",
        lastname: "bar"
    }

    test('cooledDown, should have cooled down',() =>{
        let c = new CheckIn(entity, 0.99)
        expect(c.cooledDown()).toBe(true)
    })
    
    test('cooledDown, should not have cooled down',() =>{
        let c = new CheckIn(entity, 1.01)
        expect(c.cooledDown()).toBe(false)
    })
})