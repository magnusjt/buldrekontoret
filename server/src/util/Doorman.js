let request = require('request-promise-native');

class Doorman{
    constructor(config){
        this._config = config;
    }

    async openDoor(){
        let uri = this._config.uri;
        let user = this._config.user;
        let pw = this._config.password;

        let body = `<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
          <SOAP-ENV:Body>
            <ns1:DoorGrantAccess xmlns:ns1="http://tempuri.org/ns1.xsd">
              <UserName>${user}</UserName>
              <Password>${pw}</Password>
              <doorID>4</doorID>
            </ns1:DoorGrantAccess>
          </SOAP-ENV:Body>
        </SOAP-ENV:Envelope>`;

        let options = {method: 'POST', uri, body};

        return request(options);
    }
}

module.exports = Doorman;