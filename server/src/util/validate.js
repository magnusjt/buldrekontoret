const indicative = require('indicative');

module.exports = function(data, rules, messages = {}){
    return indicative.validate(data, rules, messages)
        .catch(errors => {
            let err = errors[0];
            err.status = 400;
            throw err;
        });
};