const request = require('request-promise-native');
const crypto = require('crypto');

class Facebook{
    constructor(config){
        this._config = config;
    }

    getAppSecretProof(accessToken){
        let hmac = crypto.createHmac('sha256', this._config.appSecret);
        hmac.update(accessToken);
        return hmac.digest('hex');
    }

    async getUser(accessToken){
        let query = {
            fields: 'first_name,last_name,email',
            appsecret_proof: this.getAppSecretProof(accessToken),
            access_token: accessToken
        };

        let uri = 'https://graph.facebook.com/v2.10/me';

        let options = {method: 'GET', uri, qs: query, json: true};

        let data = await request(options);

        return {
            email: data.email,
            firstname: data.first_name,
            lastname: data.last_name,
            address: '',
            tlf: ''
        };
    }
}

module.exports = Facebook;