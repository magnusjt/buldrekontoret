const Websocket = require('websocket')
const uuid = require('uuid')

class WebSocketServer{
    constructor(config, httpServer){
        this._config = config
        this._httpServer = httpServer
        this._wsServer = null
        this._clients = {}
    }

    listen(){
        let origins = this._config.origins

        this._wsServer = new Websocket.server({
            httpServer: this._httpServer,
            autoAcceptConnections: false
        })

        function originIsAllowed(origin){
            return origins.includes(origin)
        }

        this._wsServer.on('request', req => {
            if(!originIsAllowed(req.origin)){
                console.log('Connection from origin ' + req.origin + ' rejected')
                return req.reject()
            }

            let wsConn = req.accept(null, req.origin)
            this._addConnection(wsConn)
        })
    }

    broadcast(event){
        Object.values(this._clients).map(client => {
            try{
                client.wsConn.sendUTF(JSON.stringify(event))
            }catch(err){
                console.error(err)
            }
        })
    }

    _addConnection(wsConn){
        let clientId = uuid.v4()

        this._clients[clientId] = {clientId, wsConn}

        wsConn.on('close', () => {
            delete this._clients[clientId]
            wsConn.removeAllListeners()
        })
    }
}

module.exports = WebSocketServer