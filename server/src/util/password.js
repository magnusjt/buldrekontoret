const bcrypt = require('bcrypt');

async function hash(pw){
    return bcrypt.hash(pw, 10);
}

async function verify(pw, hash){
    return bcrypt.compare(pw, hash);
}

module.exports = {hash, verify};