class Emailer{
    constructor(mailgun, config){
        this._mailgun = mailgun;
        this._config = config;
    }

    send(to, subject, html){
        return this._mailgun.messages().send({
            to,
            subject,
            html,
            from: this._config.from
        });
    }
}

module.exports = Emailer;