const knex = require('knex');

module.exports = function(config){
    let connConfig = {
        'host': config.host,
        'port': config.port,
        'user': config.user,
        'password': config.password,
        'database': config.database,
        'timezone': 'UTC' // This is the timezone that the mysql client library will assume the server is using
    };

    let pool = {
        afterCreate(conn, done){
            // Set the timezone for this connection. This must match with the timezone assumed by the client library!
            conn.query("SET time_zone='+00:00';", err => done(err, conn));
        }
    };

    return knex({client: 'mysql', connection: connConfig, pool});
};