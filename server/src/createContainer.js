const Container = require('./util/Container');
const env = require('getenv');
const http = require('http');
const path = require('path');
const EventEmitter = require('events').EventEmitter;
const express = require('express');
const bodyParser = require('body-parser');
const createMailgun = require('mailgun-js');
const registerRoutes = require('./routes');
const middleware = require('./middleware');
const connectToDatabase = require('./util/connectToDatabase');
const Emailer = require('./util/Emailer');
const Doorman = require('./util/Doorman');
const Facebook = require('./util/Facebook');
const WebSocketServer = require('./util/WebSocketServer');

const CheckInFactory = require('./models/CheckIn').CheckInFactory;

const UserRepo = require('./repos/UserRepo');
const DoorlogRepo = require('./repos/DoorlogRepo');
const CheckInRepo = require('./repos/CheckInRepo');
const PasswordResetRepo = require('./repos/PasswordResetRepo');
const CupRepo = require('./repos/CupRepo');
const CupRoundRepo = require('./repos/CupRoundRepo');
const CupRouteRepo = require('./repos/CupRouteRepo');
const CupTickRepo = require('./repos/CupTickRepo');
const GradeRepo = require('./repos/GradeRepo');
const ReservationRepo = require('./repos/ReservationRepo');

const DoormanRoute = require('./routes/DoormanRoute');
const LoginRoute = require('./routes/LoginRoute');
const RegisterRoute = require('./routes/RegisterRoute');
const ProfileRoute = require('./routes/ProfileRoute');
const CheckInRoute = require('./routes/CheckInRoute');
const PasswordResetRoute = require('./routes/PasswordResetRoute');
const CupRoute = require('./routes/CupRoute');
const CupRoundRoute = require('./routes/CupRoundRoute');
const CupRouteRoute = require('./routes/CupRouteRoute');
const CupTickRoute = require('./routes/CupTickRoute');
const GradeRoute = require('./routes/GradeRoute');
const ReservationRoute = require('./routes/ReservationRoute');

const LoginRequest = require('./validation/LoginRequest');
const RegisterRequest = require('./validation/RegisterRequest');
const PasswordResetRequest = require('./validation/PasswordResetRequest');
const CreateCupRoundRequest = require('./validation/CreateCupRoundRequest');
const CreateCupTickRequest = require('./validation/CreateCupTickRequest');
const DestroyCupTickRequest = require('./validation/DestroyCupTickRequest');
const UpdateCupRoundRequest = require('./validation/UpdateCupRoundRequest');
const CreateCupRouteRequest = require('./validation/CreateCupRouteRequest');
const UpdateCupRouteRequest = require('./validation/UpdateCupRouteRequest');
const FacebookUserDataValidator = require('./validation/FacebookUserDataValidator');
const ReservationRequest = require('./validation/ReservationRequest');

function register(ioc){
    ioc.service('CONFIG', ioc => {
        return {
            'NODE_ENV':    env.string('NODE_ENV', 'production'),
            'HOST':        env.string('HOST', 'app.buldrekontoret.com'),
            'PORT':        env.int('PORT', 80),
            'DB': {
                host:     env.string('DB_HOST', '127.0.0.1'),
                port:     env.string('DB_PORT', '3306'),
                user:     env.string('DB_USER', ''),
                password: env.string('DB_PASSWORD', ''),
                database: env.string('DB_NAME', 'doorman'),
            },
            'EMAIL': {
                method: env.string('EMAIL_METHOD', 'mailgun'),
                domain: env.string('MAILGUN_DOMAIN', 'buldrekontoret.com'),
                apiKey: env.string('MAILGUN_API_KEY', ''),
                from:   env.string('MAILGUN_FROM', 'Buldrekontoret <post@buldrekontoret.com>')
            },
            'DOORMAN': {
                user:     env.string('DOORMAN_USER', ''),
                password: env.string('DOORMAN_PASSWORD', ''),
                uri:      env.string('DOORMAN_URI', 'https://127.0.0.1:8080')
            },
            'FACEBOOK': {
                appSecret: env.string('FACEBOOK_APP_SECRET')
            },
            'WEBSOCKET': {
                origins: [
                    'http://localhost:3000',
                    'http://localhost:3001',
                    'http://app.buldrekontoret.com',
                    'http://app.buldrekontoret.com:80',
                    'https://app.buldrekontoret.com',
                    'https://app.buldrekontoret.com:443'
                ]
            },
            'SESSION_ENCRYPTION_KEY': env.string('SESSION_ENCRYPTION_KEY'),
            'REGISTRATION_SECRET':   env.string('REGISTRATION_SECRET'),
            'CHECKIN_COOLDOWN_HOURS': env.int('CHECKIN_COOLDOWN_HOURS', 4)
        };
    });

    ioc.service('HttpServer', ioc => {
        return http.createServer(ioc['App'])
    })

    ioc.service('App', ioc => {
        let app = express();

        app.use(bodyParser.json());
        app.use(bodyParser.urlencoded({ extended: false }));
        app.use('/api', middleware.logRequests());
        app.use(middleware.session(ioc['Database'], ioc['CONFIG']['SESSION_ENCRYPTION_KEY']));

        registerRoutes(ioc, app);

        app.use(express.static(path.join(__dirname, '../../client/build'), {index: false}));

        app.get('/', middleware.redirectToDashboardIfAlreadyLoggedIn);
        app.get('/*', (req, res) => res.sendFile(path.join(__dirname, '../../client/build/index.html')));

        app.use(middleware.handle404());
        app.use(middleware.handleError());

        return app;
    });

    ioc.service('WebSocketServer', ioc => {
        let server = new WebSocketServer(ioc['CONFIG']['WEBSOCKET'], ioc['HttpServer'])
        let cupEvents = ioc['CupEvents']
        cupEvents.on('TICK_CREATED', () => server.broadcast({type: 'TICK_CREATED'}))
        cupEvents.on('TICK_DESTROYED', () => server.broadcast({type: 'TICK_DESTROYED'}))
        const reservationEvents = ioc['ReservationEvents']
        reservationEvents.on('RESERVATION_CREATED', () => server.broadcast({type: 'RESERVATION_CREATED'}))
        reservationEvents.on('RESERVATION_DELETED', () => server.broadcast({type: 'RESERVATION_DELETED'}))
        return server
    })

    ioc.service('Database', ioc => {
        let config = ioc['CONFIG']['DB'];

        return connectToDatabase(config);
    });

    ioc.service('Emailer', ioc => {
        let config = ioc['CONFIG']['EMAIL'];

        if(config.method === 'console'){
            return {
                send(){console.log(arguments)}
            };
        }

        const mailgun = createMailgun({ apiKey: config.apiKey, domain: config.domain, host: 'api.eu.mailgun.net' });

        return new Emailer(mailgun, config);
    });

    ioc.service('CheckInFactory', ioc => new CheckInFactory(ioc['CONFIG']['CHECKIN_COOLDOWN_HOURS']));

    ioc.service('Doorman', ioc => new Doorman(ioc['CONFIG']['DOORMAN']));
    ioc.service('Facebook', ioc => new Facebook(ioc['CONFIG']['FACEBOOK']));
    ioc.service('CupEvents', ioc => new EventEmitter())
    ioc.service('ReservationEvents', ioc => new EventEmitter())

    ioc.service('UserRepo', ioc => new UserRepo(ioc['Database']));
    ioc.service('DoorlogRepo', ioc => new DoorlogRepo(ioc['Database']));
    ioc.service('CheckInRepo', ioc => new CheckInRepo(ioc['Database'], ioc['CheckInFactory']));
    ioc.service('PasswordResetRepo', ioc => new PasswordResetRepo(ioc['Database']));
    ioc.service('CupRepo', ioc => new CupRepo(ioc['Database']));
    ioc.service('CupRoundRepo', ioc => new CupRoundRepo(ioc['Database']));
    ioc.service('CupRouteRepo', ioc => new CupRouteRepo(ioc['Database']));
    ioc.service('CupTickRepo', ioc => new CupTickRepo(ioc['Database'], ioc['CupEvents']));
    ioc.service('GradeRepo', ioc => new GradeRepo(ioc['Database']));
    ioc.service('ReservationRepo', ioc => new ReservationRepo(ioc['Database'], ioc['ReservationEvents']));

    ioc.service('DoormanRoute', ioc => new DoormanRoute(ioc['Doorman'], ioc['DoorlogRepo']));
    ioc.service('LoginRoute', ioc => new LoginRoute(ioc['UserRepo'], ioc['Emailer'], ioc['Facebook'], ioc['FacebookUserDataValidator']));
    ioc.service('RegisterRoute', ioc => new RegisterRoute(ioc['UserRepo'], ioc['Emailer']));
    ioc.service('ProfileRoute', ioc => new ProfileRoute(ioc['UserRepo']));
    ioc.service('CheckInRoute', ioc => new CheckInRoute(ioc['CheckInRepo']));
    ioc.service('PasswordResetRoute', ioc => new PasswordResetRoute(ioc['UserRepo'], ioc['PasswordResetRepo'], ioc['Emailer']));
    ioc.service('CupRoute', ioc => new CupRoute(ioc['CupRepo'], ioc['CupRoundRepo'], ioc['CupRouteRepo'], ioc['CupTickRepo']));
    ioc.service('CupRoundRoute', ioc => new CupRoundRoute(ioc['CupRoundRepo']));
    ioc.service('CupRouteRoute', ioc => new CupRouteRoute(ioc['CupRouteRepo']));
    ioc.service('CupTickRoute', ioc => new CupTickRoute(ioc['CupTickRepo']));
    ioc.service('GradeRoute', ioc => new GradeRoute(ioc['GradeRepo']));
    ioc.service('ReservationRoute', ioc => new ReservationRoute(ioc['ReservationRepo']));

    ioc.service('LoginRequest', ioc => new LoginRequest());
    ioc.service('RegisterRequest', ioc => new RegisterRequest(ioc['UserRepo'], ioc['CONFIG']['REGISTRATION_SECRET']));
    ioc.service('PasswordResetRequest', ioc => new PasswordResetRequest());
    ioc.service('CreateCupRoundRequest', ioc => new CreateCupRoundRequest(ioc['CupRepo']));
    ioc.service('UpdateCupRoundRequest', ioc => new UpdateCupRoundRequest(ioc['CupRepo']));
    ioc.service('CreateCupTickRequest', ioc => new CreateCupTickRequest(ioc['CupRoundRepo'], ioc['CupRouteRepo']));
    ioc.service('DestroyCupTickRequest', ioc => new DestroyCupTickRequest(ioc['CupRoundRepo'], ioc['CupRouteRepo'], ioc['CupTickRepo']));
    ioc.service('CreateCupRouteRequest', ioc => new CreateCupRouteRequest(ioc['CupRoundRepo'], ioc['GradeRepo']));
    ioc.service('UpdateCupRouteRequest', ioc => new UpdateCupRouteRequest(ioc['GradeRepo']));
    ioc.service('FacebookUserDataValidator', ioc => new FacebookUserDataValidator());
    ioc.service('ReservationRequest', ioc => new ReservationRequest(ioc['ReservationRepo']));
}

module.exports = function(){
    let ioc = new Container();
    register(ioc);
    return ioc;
};