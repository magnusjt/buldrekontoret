function handle404(){
    return function(req, res, next){
        let err = new Error('Ikke funnet');
        err.status = 404;
        next(err);
    }
}

function handleError(){
    return function(err, req, res, next){
        console.error(new Date(), err);

        res.status(err.status || 500);

        if(err.status && err.status >= 400 && err.status < 500){
            return res.json({error: err.message});
        }

        return res.json({error: 'En uventet feil oppstod'});
    }
}

function logRequests(){
    return function(req, res, next){
        console.log(new Date(), req.url);
        next();
    }
}

function validate(validator){
    return function(req, res, next){
        validator(req)
            .then(() => {
                next();
            })
            .catch(err => {
                next(err);
            });
    }
}

function route(route){
    return function(req, res, next){
        try{
            route(req, res).catch(next);
        }catch(err){
            next(err);
        }
    }
}

function session(knex, encryptionKey){
    const session = require('express-session');
    const KnexSessionStore = require('connect-session-knex')(session);
    const store = new KnexSessionStore({
        tablename: 'sessions',
        sidfieldname: 'sid',
        createtable: false,
        clearInterval: 60000,
        knex
    });

    return session({
        secret: encryptionKey,
        resave: false,
        saveUninitialized: false,
        cookie: {
            httpOnly: true,
            maxAge: false
        },
        store: store
    });
}

function isLoggedIn(req){
    return (req.session && req.session.userId);
}

function requireLoggedIn(userRepo){
    return function(req, res, next){
        if(!isLoggedIn(req)){
            let err = new Error('Du må være logget inn');
            err.status = 403;
            return next(err);
        }

        userRepo.getById(req.session.userId)
            .then(user => {
                req.user = user;
                next();
            })
            .catch(next);
    }
}

function requireAdmin(req, res, next){
    if(!req.user.isAdmin){
        let err = new Error('Din bruker mangler admin rettigheter');
        err.status = 401;
        return next(err);
    }
    next(); 
}

function redirectToDashboardIfAlreadyLoggedIn(req, res, next){
    if(isLoggedIn(req) && req.originalUrl !== '/dashboard'){
        return res.redirect('/dashboard');
    }
    next();
}

function redirectToHomeIfNotLoggedIn(req, res, next){
    if(!isLoggedIn(req) && req.originalUrl !== '/'){
        return res.redirect('/');
    }
    next();
}


module.exports  ={
    handle404,
    handleError,
    logRequests,
    validate,
    route,
    session,
    requireLoggedIn,
    requireAdmin,
    redirectToDashboardIfAlreadyLoggedIn,
    redirectToHomeIfNotLoggedIn
};