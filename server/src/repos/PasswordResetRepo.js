const crypto = require('crypto');
const moment = require('moment');
const _ = require('lodash');

class PasswordResetRepo{
    constructor(db){
        this._db = db;

        this._columns = [
            'userId',
            'token',
            'createdAt'
        ];
    }

    async create(userId){
        let token = crypto.randomBytes(32).toString('hex');
        let data = {userId, token};

        await this._db
            .insert(data)
            .into('passwordResets');

        return data;
    }

    async get(token){
        let rows = await this._db
            .select(this._columns)
            .from('passwordResets')
            .where('token', token)
            .where('createdAt', '>=', moment().subtract(1, 'h').toDate());

        if(rows.length === 0){
            let err = new Error('Ugyldig kode');
            err.status = 400;
            throw err;
        }

        return rows[0];
    }

    async remove(token){
        return this._db
            .delete()
            .from('passwordResets')
            .where('token', token);
    }
}

module.exports = PasswordResetRepo;