const _ = require('lodash');

class CupTickRepo{
    /**
     * @param db
     * @param {EventEmitter} cupEvents
     */
    constructor(db, cupEvents){
        this._db = db;
        this._cupEvents = cupEvents

        this._columns = [
            'cupTicks.id AS id',
            'cupTicks.userId AS userId',
            'cupTicks.routeId AS routeId',
            'cupTicks.createdAt AS createdAt',
            'cupTicks.updatedAt AS updatedAt',
            'users.firstname AS firstname',
            'users.lastname AS lastname'
        ];

        this._saveable = [
            'userId',
            'routeId',
        ];

        this._table = 'cupTicks';
    }

    async getByRouteIds(routeIds){
        return await this._db
            .select(this._columns)
            .from(this._table)
            .join('users', 'cupTicks.userId', 'users.id')
            .whereIn('routeId', routeIds);
    }

    async getByIdOrNull(id){
        let rows =  await this._db
            .select(this._columns)
            .from(this._table)
            .join('users', 'cupTicks.userId', 'users.id')
            .where('cupTicks.id', id);

        if(rows.length === 0){
            return null;
        }

        return rows[0];
    }

    async getById(id){
        let tick = await this.getByIdOrNull(id);

        if(tick === null){
            let err = new Error('Ikke funnet');
            err.status = 404;
            throw err;
        }

        return tick;
    }

    async getByUserIdAndRouteId(userId, routeId){
        let rows = await this._db
            .select(this._columns)
            .from(this._table)
            .join('users', 'cupTicks.userId', 'users.id')
            .where('userId', userId)
            .where('routeId', routeId);

        if(rows.length === 0){
            return null;
        }

        return rows[0];
    }

    async create(data){
        let tick = await this.getByUserIdAndRouteId(data.userId, data.routeId);
        if(tick !== null){
            return tick;
        }

        data = _.pick(data, this._saveable);

        let [id] = await this._db
            .insert(data)
            .into(this._table);

        this._cupEvents.emit('TICK_CREATED', id)

        return id;
    }

    async destroy(userId, tickId){
        await this._db
            .delete()
            .from(this._table)
            .where('userId', userId)
            .where('id', tickId);

        this._cupEvents.emit('TICK_DESTROYED', tickId)
    }
}

module.exports = CupTickRepo;