const _ = require('lodash');

class CupRepo{
    constructor(db){
        this._db = db;

        this._columns = [
            'id',
            'name',
            'startsAt',
            'endsAt',
            'isCurrent',
            'createdAt',
            'updatedAt',
        ];

        this._saveable = [
            'name',
            'startsAt',
            'endsAt',
            'isCurrent'
        ];

        this._table = 'cups';
    }

    async getCurrent(){
        let rows = await this._db
            .select(this._columns)
            .from(this._table)
            .where('isCurrent', true)
            .orderBy('startsAt', 'desc');

        if(rows.length === 0){
            return null;
        }

        return rows[0];
    }

    async getByIdOrNull(id){
        let rows = await this._db
            .select(this._columns)
            .from(this._table)
            .where('id', id);

        if(rows.length === 0){
            return null;
        }

        return rows[0];
    }

    async getById(id){
        let cup = await this.getByIdOrNull(id);

        if(cup === null){
            let err = new Error('Kunne ikke finne sesong');
            err.status = 404;
            throw err;
        }

        return cup;
    }

    async getAll(){
        return this._db
            .select(this._columns)
            .from(this._table)
            .orderBy('startsAt', 'desc');
    }

    async create(data){
        data = _.pick(data, this._saveable);

        let [id] = await this._db
            .insert(data)
            .into(this._table);

        return id;
    }

    update(data, id){
        data = _.pick(data, this._saveable);

        return this._db
            .update(data)
            .into(this._table)
            .where('id', id);
    }

    destroy(id){
        return this._db
            .delete()
            .from(this._table)
            .where('id', id);
    }
}

module.exports = CupRepo;