const moment = require('moment');

class DoorlogRepo{
    constructor(db){
        this._db = db;

        this._entryColumns = [
            'userId',
            'email',
            'firstname',
            'lastname',
            'openedAt'
        ];
    }

    logDoorOpened(userId){
        return this._db
            .insert({userId})
            .into('doorlog');
    }

    async lastOpenedAt(){
        let rows = await this._db
            .select('openedAt')
            .from('doorlog')
            .orderBy('openedAt', 'desc')
            .limit(1);

        if(rows.length === 0){
            return null;
        }

        return moment(rows[0]['openedAt']);
    }

    async getDoorlogEntries(){
        let entities = await this._db
            .from('doorlog')
            .join('users', 'doorlog.userId', 'users.id')
            .select(this._entryColumns)
            .orderBy('openedAt', 'desc')
            .limit(50);
            
        return entities;
    }
}

module.exports = DoorlogRepo;