class ReservationRepo{
    constructor(db, events){
        this._db = db;
        this._events = events;
    }

    async getById(id){
        const rows = await this._db
            .select('*')
            .from('reservations')
            .where('id', id)

        if(rows.length !== 1){
            const err = new Error('Reservation not found')
            err.status = 404
            throw err
        }

        return rows[0]
    }

    async getForRange(start, end){
        return this._db
            .select('*')
            .from('reservations')
            .where(q => q
                .where('startsAt', '>=', start)
                .where('startsAt', '<', end)
            )
            .orWhere(q => q
                .where('endsAt', '>', start)
                .where('endsAt', '<=', end)
            )
            .orderBy('startsAt', 'asc')
    }

    async getByUserFrom(userId, start){
        return this._db
            .select('*')
            .from('reservations')
            .where('userId', userId)
            .where('startsAt', '>=', start)
            .orderBy('startsAt', 'asc')
    }

    async create(data){
        const [id] = await this._db
            .insert(data)
            .into('reservations')

        this._events.emit('RESERVATION_CREATED')

        return id
    }

    async remove(id, userId){
        const res = await this._db
            .delete()
            .from('reservations')
            .where('id', id)
            .where('userId', userId)

        this._events.emit('RESERVATION_DELETED')

        return res
    }

    async adminDump(includeSensitiveData){
        return this._db
            .select([
                'reservations.startsAt as startsAt',
                'reservations.endsAt as endsAt',
                'users.firstname as firstname',
                'users.lastname as lastname',
                ...(includeSensitiveData ? [
                    'users.email as email',
                    'users.address as address',
                    'users.tlf as tlf'
                ] : [])
            ])
            .from('reservations')
            .join('users', 'users.id', 'reservations.userId')
    }
}

module.exports = ReservationRepo;