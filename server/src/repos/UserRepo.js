const pwUtil = require('../util/password');
const crypto = require('crypto');
const _ = require('lodash');

class UserRepo{
    constructor(db){
        this._db = db;

        this._columns = [
            'id',
            'email',
            'firstname',
            'lastname',
            'tlf',
            'address',
            'password',
            'isAdmin',
            'hasAcceptedTerms',
            'confirmed',
            'confirmationCode',
            'createdAt',
            'updatedAt'
        ];

        this._saveable = [
            'email',
            'firstname',
            'lastname',
            'tlf',
            'address',
            'password',
            'confirmationCode',
            'hasAcceptedTerms'
        ];
    }

    async create(data, password){
        data.password = await pwUtil.hash(password);
        data.confirmed = false;
        data.confirmationCode = crypto.randomBytes(32).toString('hex');

        data = _.pick(data, this._saveable);

        let [id] = await this._db
            .insert(data)
            .into('users');

        return await this.getById(id);
    }

    async createUserWithRandomPassword(data){
        let password = crypto.randomBytes(32).toString('hex');
        return this.create(data, password)
    }

    async setConfirmed(id){
        await this._db
            .update({confirmed: true})
            .into('users')
            .where('id', id);
    }

    async getByConfirmationCode(confirmationCode){
        let rows = await this._db
            .select(this._columns)
            .from('users')
            .where('confirmationCode', confirmationCode);

        if(rows.length !== 1){
            let err = new Error('Ugyldig kode');
            err.status = 400;
            throw err;
        }

        return rows[0]
    }

    async changePassword(userId, passwordPlain){
        let password = await pwUtil.hash(passwordPlain);

        return this._db
            .update({password})
            .into('users')
            .where('id', userId);
    }

    async exists(email){
        let rows = await this._db
            .select('id')
            .from('users')
            .where('email', email);

        return rows.length > 0;
    }

    async verifyCredentials(email, password){
        let rows = await this._db
            .select(this._columns)
            .from('users')
            .where('email', email)
            .where('confirmed', true);

        if(rows.length === 0){
            return false;
        }

        let hash = rows[0].password;
        return pwUtil.verify(password, hash);
    }

    async getByEmailOrNull(email){
        let rows = await this._db
            .select(this._columns)
            .from('users')
            .where('email', email);

        if(rows.length === 0){
            return null;
        }

        return rows[0];
    }

    async getByEmail(email){
        let user = await this.getByEmailOrNull(email);

        if(user === null){
            let err = new Error('Kunne ikke finne bruker');
            err.status = 404;
            throw err;
        }

        return user;
    }

    async getById(id){
        let rows = await this._db
            .select(this._columns)
            .from('users')
            .where('id', id);

        if(rows.length === 0){
            let err = new Error('Kunne ikke finne bruker');
            err.status = 404;
            throw err;
        }

        return rows[0];
    }

    async acceptTerms(id){
        const affected = await this._db
            .update({ hasAcceptedTerms: 1 })
            .into('users')
            .where('id', id)

        if(affected === 0){
            let err = new Error('Kunne ikke finne bruker');
            err.status = 404;
            throw err;
        }
    }
}

module.exports = UserRepo;