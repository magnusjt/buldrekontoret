const _ = require('lodash');

class CupRoundRepo{
    constructor(db){
        this._db = db;

        this._columns = [
            'id',
            'name',
            'startsAt',
            'endsAt',
            'cupId',
            'createdAt',
            'updatedAt',
        ];

        this._saveable = [
            'name',
            'startsAt',
            'endsAt',
            'cupId',
        ];

        this._updatable = [
            'name',
            'startsAt',
            'endsAt',
        ];

        this._table = 'cupRounds';
    }

    async getByCupId(cupId){
        return this._db
            .select(this._columns)
            .from(this._table)
            .where('cupId', cupId)
            .orderBy('startsAt', 'desc');
    }

    async getByIdOrNull(id){
        let rows =  await this._db
            .select(this._columns)
            .from(this._table)
            .where('id', id);

        if(rows.length === 0){
            return null;
        }

        return rows[0];
    }

    async getById(id){
        let round = await this.getByIdOrNull(id);

        if(round === null){
            let err = new Error('Ikke funnet');
            err.status = 404;
            throw err;
        }

        return round;
    }

    async create(data){
        data = _.pick(data, this._saveable);

        let [id] = await this._db
            .insert(data)
            .into(this._table);

        return id;
    }

    update(data, id){
        data = _.pick(data, this._updatable);

        return this._db
            .update(data)
            .into(this._table)
            .where('id', id);
    }

    destroy(id){
        return this._db
            .delete()
            .from(this._table)
            .where('id', id);
    }
}

module.exports = CupRoundRepo;