const _ = require('lodash');
const moment = require('moment');

class CheckInRepo{
    /**
     * @param db
     * @param {CheckInFactory} checkInFactory
     */
    constructor(db, checkInFactory){
        this._db = db;
        this._checkInFactory = checkInFactory;
        
        this._columns = [
            'checkIn.id AS id',
            'userId',
            'checkedIn',
            'firstname',
            'lastname'
        ];

        this._saveable = [
            'userId'
        ];
    }
    
    async checkIn(userId){     
        return await this._db
            .insert({userId})
            .into('checkIn');
    }

    async getLastCheckIn(userId){
        let entities = await this._db
            .from('checkIn')
            .join('users', 'checkIn.userId', 'users.id')
            .select(this._columns)
            .orderBy('checkedIn', 'desc')
            .where('userId', userId)
            .limit(1);
        
        if(entities.length === 0)
            return null;

        return this._checkInFactory.create(entities[0]);
    }

    async getById(id){
        let entities = await this._db
            .from('checkIn')
            .join('users', 'checkIn.userId', 'users.id')
            .select(this._columns)
            .where('checkIn.id', id);
        
        if(entities.length === 0)
            return null;

        return this._checkInFactory.create(entities[0]);
    }

    async getCheckIns(afterTime){
        afterTime = afterTime || moment("12/12/2012","DD/MM/YYYY").toDate();
        let entities = await this._db
            .from('checkIn')
            .join('users', 'checkIn.userId', 'users.id')
            .select(this._columns)
            .where('checkedIn','>', afterTime)
            .orderBy('checkedIn', 'asc');

        return entities.map(entity => this._checkInFactory.create(entity));
    }

    async getLastCheckIns(afterTime){
        return _(await this.getCheckIns(afterTime))
            .keyBy(stamp => stamp.userId)
            .orderBy(stamp => stamp.checkedIn, 'desc')
            .toArray();
    }    
}

module.exports = CheckInRepo;