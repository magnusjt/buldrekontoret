const _ = require('lodash');

class GradeRepo{
    constructor(db){
        this._db = db;
    }

    async getAll(){
        let columns = [
            'grades.id AS id',
            'grades.name AS name',
            'grades.sortIndex AS sortIndex',
            'routeColors.name AS colorName',
            'routeColors.color AS color',
            'routeColors.description AS description',
            'routeColors.sortIndex AS colorSortIndex'
        ];

        return await this._db
            .select(columns)
            .from('grades')
            .join('routeColors', 'grades.colorId', 'routeColors.id')
            .orderBy('grades.sortIndex', 'asc');
    }

    async exists(id){
        let rows = await this._db
            .select('*')
            .from('grades')
            .where('id', id);

        return rows.length > 0;
    }
}

module.exports = GradeRepo;