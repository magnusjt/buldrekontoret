const _ = require('lodash');

class CupRouteRepo{
    constructor(db){
        this._db = db;

        this._columns = [
            'id',
            'number',
            'roundId',
            'gradeId',
            'createdAt',
            'updatedAt',
        ];

        this._saveable = [
            'number',
            'roundId',
            'gradeId',
        ];

        this._updatable = [
            'number',
            'gradeId',
        ];

        this._table = 'cupRoutes';
    }

    async getByRoundIds(roundIds){
        return this._db
            .select(this._columns)
            .from(this._table)
            .whereIn('roundId', roundIds);
    }

    async exists(id){
        let rows =  await this._db
            .select(this._columns)
            .from(this._table)
            .where('id', id);

        return rows.length > 0;
    }

    async getById(id){
        let rows =  await this._db
            .select(this._columns)
            .from(this._table)
            .where('id', id);

        if(rows.length === 0){
            let err = new Error('Ikke funnet');
            err.status = 404;
            throw err;
        }

        return rows[0];
    }

    async create(data){
        data = _.pick(data, this._saveable);

        let [id] = await this._db
            .insert(data)
            .into(this._table);

        return id;
    }

    update(data, id){
        data = _.pick(data, this._updatable);

        return this._db
            .update(data)
            .into(this._table)
            .where('id', id);
    }

    destroy(id){
        return this._db
            .delete()
            .from(this._table)
            .where('id', id);
    }
}

module.exports = CupRouteRepo;