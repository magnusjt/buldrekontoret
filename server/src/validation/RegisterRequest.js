const validate = require('../util/validate');
const createValidationMessages = require('./createValidationMessages');
const _ = require('lodash');

class RegisterRequest{
    constructor(userRepo, registrationSecret){
        this._userRepo = userRepo;
        this._registrationSecret = registrationSecret;

        this._rules = {
            email:           'required|email|max:200',
            firstname:       'required|min:2|max:30',
            lastname:        'required|min:2|max:30',
            tlf:             'required|min:8|max:20',
            address:         'required|min:4|max:200',
            code:            'required',
            password:        'required|min:8',
            passwordRepeat:  'required|same:password'
        };

        let trans = {
            'email':           'Epost',
            'firstname':       'Fornavn',
            'lastname':        'Etternavn',
            'tlf':             'Telefonnr',
            'address':         'Adresse',
            'code':            'Hemmelig kode',
            'password':        'Passord',
            'passwordRepeat':  'Repetert passord',
        };

        this._messages = createValidationMessages(trans);
    }

    async validate(req){
        if(_.get(req.body, 'code', '').toLowerCase() !== this._registrationSecret){
            let err = new Error('Feil kode');
            err.status = 400;
            throw err;
        }

        await validate(req.body, this._rules, this._messages);

        if(await this._userRepo.exists(req.body.email)){
            let err = new Error('Bruker er allerede registrert');
            err.status = 400;
            throw err;
        }
    }
}

module.exports = RegisterRequest;