const validate = require('../util/validate');
const _ = require('lodash');
const createValidationMessages = require('./createValidationMessages');

class PasswordResetRequest{
    constructor(){
        this._rules = {
            password:        'required|min:8',
            passwordRepeat:  'required|same:password'
        };

        let trans = {
            'password':        'Passord',
            'passwordRepeat':  'Repetert passord',
        };

        this._messages = createValidationMessages(trans);
    }

    async validate(req){
        await validate(req.body, this._rules, this._messages);
    }
}

module.exports = PasswordResetRequest;