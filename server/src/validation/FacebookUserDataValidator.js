const validate = require('../util/validate');

class FacebookUserDataValidator{
    constructor(){
        this._rules = {
            email:     'required|email|max:200',
            firstname: 'required|min:2|max:40',
            lastname:  'required|min:2|max:40'
        };

        let help = 'Sjekk din facebook-konto, eller registrer deg på vanlig måte';

        this._messages = {
            'email.required': 'Kunne ikke finne noen e-post hos facebook. ' + help,
            'email.email': 'E-posten du har lagt inn på facebook ser ikke ut til å være en gyldig e-post. ' + help,
            'email.max': 'E-posten du har lagt inn på facebook har for mange tegn. Maks er 200 tegn. ' + help,
            'firstname.required': 'Kunne ikke finne ditt fornavn i informasjonen fra facebook. ' + help,
            'firstname.min': 'Fornavnet ditt på facebook er for kort. Minimum er 2 tegn. ' + help,
            'firstname.max': 'Fornavnet ditt på facebook er for langt. Maks er 40 tegn. ' + help,
            'lastname.required': 'Kunne ikke finne ditt etternavn i informasjonen fra facebook. ' + help,
            'lastname.min': 'Etternavnet ditt på facebook er for kort. Minimum er 2 tegn. ' + help,
            'lastname.max': 'Etternavnet ditt på facebook er for langt. Maks er 40 tegn. ' + help
        };
    }

    async validate(data){
        await validate(data, this._rules, this._messages);
    }
}

module.exports = FacebookUserDataValidator;