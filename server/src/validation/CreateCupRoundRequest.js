const validate = require('../util/validate');
const createValidationMessages = require('./createValidationMessages');

class CreateCupRoundRequest{
    /**
     * @param {CupRepo} cupRepo
     */
    constructor(cupRepo){
        this._cupRepo = cupRepo;

        this._rules = {
            name: 'required|min:2|max:100',
            startsAt: 'required',
            endsAt: 'required',
            cupId: 'required'
        };

        let trans = {
            name: 'Navn',
            startsAt: 'Start',
            endsAt: 'Slutt',
            cupId: 'Cup',
        };

        this._messages = createValidationMessages(trans);
    }

    async validate(req){
        await validate(req.body, this._rules, this._messages);

        let cup = await this._cupRepo.getByIdOrNull(req.body.cupId);

        if(cup === null){
            let err = new Error('Cup\'en finnes ikke');
            err.status = 400;
            throw err;
        }
    }
}

module.exports = CreateCupRoundRequest;