const moment = require('moment');
const validate = require('../util/validate');
const validateTickingIsOpen = require('./validateTickingIsOpen');
const createValidationMessages = require('./createValidationMessages');

class CreateCupTickRequest{
    /**
     * @param {CupRoundRepo} cupRoundRepo
     * @param {CupRouteRepo} cupRouteRepo
     */
    constructor(cupRoundRepo, cupRouteRepo){
        this._cupRoundRepo = cupRoundRepo;
        this._cupRouteRepo = cupRouteRepo;

        this._rules = {
            routeId: 'required'
        };

        let trans = {
            'routeId': 'Rute'
        };

        this._messages = createValidationMessages(trans);
    }

    async validate(req){
        await validate(req.body, this._rules, this._messages);

        let routeExists = await this._cupRouteRepo.exists(req.body.routeId);

        if(!routeExists){
            let err = new Error('Fant ingen rute å ticke');
            err.status = 400;
            throw err;
        }

        let route = await this._cupRouteRepo.getById(req.body.routeId);
        let round = await this._cupRoundRepo.getByIdOrNull(route.roundId);
        validateTickingIsOpen(round, req.user);
    }
}

module.exports = CreateCupTickRequest;