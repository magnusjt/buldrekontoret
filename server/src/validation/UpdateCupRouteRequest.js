const validate = require('../util/validate');
const createValidationMessages = require('./createValidationMessages');

class UpdateCupRouteRequest{
    /**
     * @param {GradeRepo} gradeRepo
     */
    constructor(gradeRepo){
        this._gradeRepo = gradeRepo;

        this._rules = {
            number: 'min:1|max:5'
        };

        let trans = {
            number: 'Rutenummer'
        };

        this._messages = createValidationMessages(trans);
    }

    async validate(req){
        await validate(req.body, this._rules, this._messages);

        if(req.body.gradeId){
            let gradeExists = await this._gradeRepo.exists(req.body.gradeId);

            if (!gradeExists) {
                let err = new Error('Graden finnes ikke');
                err.status = 400;
                throw err;
            }
        }
    }
}

module.exports = UpdateCupRouteRequest;