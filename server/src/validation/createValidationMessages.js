module.exports = function(translations){
    return {
        'required': (field) => `${translations[field]} er påkrevd`,
        'email': (field) => `${translations[field]} må være en gyldig epost`,
        'min': (field, validation, args) => `${translations[field]} må være minimum ${args} tegn`,
        'max': (field, validation, args) => `${translations[field]} må være maksimum ${args} tegn`,
        'same': (field, validation, args) => `${translations[field]} må være likt ${translations[args]}`
    };
};