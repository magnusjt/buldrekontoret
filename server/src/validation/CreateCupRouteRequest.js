const validate = require('../util/validate');
const createValidationMessages = require('./createValidationMessages');

class CreateCupRouteRequest{
    /**
     * @param {CupRoundRepo} roundRepo
     * @param {GradeRepo} gradeRepo
     */
    constructor(roundRepo, gradeRepo){
        this._roundRepo = roundRepo;
        this._gradeRepo = gradeRepo;

        this._rules = {
            number: 'required|min:1|max:5',
            roundId: 'required',
            gradeId: 'required'
        };

        let trans = {
            number: 'Rutenummer',
            roundId: 'Rune',
            gradeId: 'Grad',
        };

        this._messages = createValidationMessages(trans);
    }

    async validate(req){
        await validate(req.body, this._rules, this._messages);

        let cupRound = await this._roundRepo.getByIdOrNull(req.body.roundId);

        if(cupRound === null){
            let err = new Error('Omgangen finnes ikke');
            err.status = 400;
            throw err;
        }

        let gradeExists = await this._gradeRepo.exists(req.body.gradeId);

        if(!gradeExists){
            let err = new Error('Graden finnes ikke');
            err.status = 400;
            throw err;
        }
    }
}

module.exports = CreateCupRouteRequest;