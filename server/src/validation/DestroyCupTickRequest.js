const validateTickingIsOpen = require('./validateTickingIsOpen');

class DestroyCupTickRequest{
    /**
     * @param {CupRoundRepo} cupRoundRepo
     * @param {CupRouteRepo} cupRouteRepo
     * @param {cupTickRepo} cupTickRepo
     */
    constructor(cupRoundRepo, cupRouteRepo, cupTickRepo) {
        this._cupRoundRepo = cupRoundRepo;
        this._cupRouteRepo = cupRouteRepo;
        this._cupTickRepo = cupTickRepo;
    }

    async validate(req){
        let tick = await this._cupTickRepo.getByIdOrNull(req.params.tickId);

        if(tick === null){
            let err = new Error('Ingenting å slette');
            err.status = 400;
            throw err;
        }

        if(tick.userId !== req.user.id){
            let err = new Error('Du har ikke tilgang til å slette denne ticken');
            err.status = 400;
            throw err;
        }

        let route = await this._cupRouteRepo.getById(tick.routeId);
        let round = await this._cupRoundRepo.getByIdOrNull(route.roundId);
        validateTickingIsOpen(round, req.user);
    }
}

module.exports = DestroyCupTickRequest;