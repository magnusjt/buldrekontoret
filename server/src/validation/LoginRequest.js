const validate = require('../util/validate');
const createValidationMessages = require('./createValidationMessages');

class LoginRequest{
    constructor(){
        this._rules = {
            email:    'required|email',
            password: 'required|min:8'
        };

        let trans = {
            'email':    'Epost',
            'password': 'Passord'
        };

        this._messages = createValidationMessages(trans);
    }

    async validate(req){
        return validate(req.body, this._rules, this._messages);
    }
}

module.exports = LoginRequest;