const moment = require('moment');
const _ = require('lodash')

const makeError = (msg) => {
    const err = new Error(msg)
    err.status = 400
    return err
}

class ReservationRequest{
    /**
     * @param {ReservationRepo} reservationRepo
     */
    constructor(reservationRepo){
        this._reservationRepo = reservationRepo
    }

    async validate(req){
        const startsAt = new Date(req.body.startsAt)
        const endsAt = new Date(req.body.endsAt)

        if(startsAt >= endsAt){
            throw makeError('Start must be before end')
        }
        if(moment(endsAt).diff(moment(startsAt), 'minutes') > 120){
            throw makeError('Cannot reserve more than 2 hours')
        }
        if(startsAt <= new Date()){
            throw makeError('Cannot reserve back in time')
        }
        if(moment(startsAt).isAfter(moment().add(3, 'weeks'))){
            throw makeError('Cannot reserve more than 3 weeks in the future')
        }

        const myActiveReservations = await this._reservationRepo.getByUserFrom(req.user.id, new Date())
        if(myActiveReservations.length >= 3){
            throw makeError('Can only have 3 active reservations')
        }

        // Not quite accurate in all timezones, but good enough since no-one reserves at 2 am
        const myReservations = await this._reservationRepo.getByUserFrom(req.user.id, moment().startOf('day').toDate())
        if(myReservations.some(r => moment(r.startsAt).format('YYYY-MM-DD') === moment(startsAt).format('YYYY-MM-DD'))){
            throw makeError('A reservation already exists for this date')
        }
    }

    async validateDeleteRequest(req){
        const reservation = await this._reservationRepo.getById(req.params.id)
        if(reservation.startsAt < new Date()){
            throw makeError('Cannot delete reservation that\'s already started')
        }
    }
}

module.exports = ReservationRequest;