const moment = require('moment');

module.exports = function(cupRound, user){
    if(user.isAdmin){
        return;
    }

    let now = moment();

    if(now < moment(cupRound.startsAt)){
        let err = new Error('Du er litt tidlig ute med tickingen. Prøv igjen senere');
        err.status = 400;
        throw err;
    }

    if(now > moment(cupRound.endsAt)){
        let err = new Error('Du er dessverre for sent ute. Prøv igjen neste cup');
        err.status = 400;
        throw err;
    }
};