const validate = require('../util/validate');
const createValidationMessages = require('./createValidationMessages');

class UpdateCupRoundRequest{
    /**
     * @param {CupRepo} cupRepo
     */
    constructor(cupRepo){
        this._cupRepo = cupRepo;

        this._rules = {
            name: 'min:2|max:100'
        };

        let trans = {
            name: 'Navn'
        };

        this._messages = createValidationMessages(trans);
    }

    async validate(req){
        await validate(req.body, this._rules, this._messages);
    }
}

module.exports = UpdateCupRoundRequest;