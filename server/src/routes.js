const route = require('./middleware').route;
const validate = require('./middleware').validate;
const createRequireLoggedIn = require('./middleware').requireLoggedIn;
const requireAdmin = require('./middleware').requireAdmin;

const express = require('express');

module.exports = function(ioc, app){
    /** @var {DoormanRoute} doormanRoute */
    const doormanRoute = ioc['DoormanRoute'];

    /** @var {LoginRoute} loginRoute */
    const loginRoute = ioc['LoginRoute'];

    /** @var {RegisterRoute} registerRoute */
    const registerRoute = ioc['RegisterRoute'];

    /** @var {ProfileRoute} profileRoute */
    const profileRoute = ioc['ProfileRoute'];

    /** @var {PasswordResetRoute} passwordResetRoute */
    const passwordResetRoute = ioc['PasswordResetRoute'];

    /** @var {CupRoute} cupRoute */
    const cupRoute = ioc['CupRoute'];

    /** @var {CupRoundRoute} cupRoundRoute */
    const cupRoundRoute = ioc['CupRoundRoute'];

    /** @var {CupRouteRoute} cupRouteRoute */
    const cupRouteRoute  = ioc['CupRouteRoute'];

    /** @var {CupTickRoute} cupTickRoute */
    const cupTickRoute  = ioc['CupTickRoute'];

    /** @var {GradeRoute} gradeRoute */
    const gradeRoute = ioc['GradeRoute'];

    /** @var {LoginRequest} loginRequest */
    const loginRequest = ioc['LoginRequest'];

    /** @var {RegisterRequest} registerRequest */
    const registerRequest = ioc['RegisterRequest'];

    /** @var {PasswordResetRequest} passwordResetRequest */
    const passwordResetRequest = ioc['PasswordResetRequest'];

    /** @var {CreateCupRoundRequest} createCupRoundRequest */
    const createCupRoundRequest = ioc['CreateCupRoundRequest'];

    /** @var {UpdateCupRoundRequest} updateCupRoundRequest */
    const updateCupRoundRequest = ioc['UpdateCupRoundRequest'];

    /** @var {CreateCupTickRequest} createCupTickRequest */
    const createCupTickRequest = ioc['CreateCupTickRequest'];

    /** @var {DestroyCupTickRequest} destroyCupTickRequest */
    const destroyCupTickRequest = ioc['DestroyCupTickRequest'];

    /** @var {CreateCupRouteRequest} createCupRouteRequest */
    const createCupRouteRequest = ioc['CreateCupRouteRequest'];

    /** @var {UpdateCupRouteRequest} updateCupRouteRequest */
    const updateCupRouteRequest = ioc['UpdateCupRouteRequest'];

    /** @var {CheckInRoute} checkInRoute */
    const checkInRoute = ioc['CheckInRoute'];

    const requireLoggedIn = createRequireLoggedIn(ioc['UserRepo']);

    /** @var {ReservationRoute} reservationRoute */
    const reservationRoute = ioc['ReservationRoute']

    /** @var {ReservationRequest} reservationRequest */
    const reservationRequest = ioc['ReservationRequest']

    app.post('/api/opendoor',
        requireLoggedIn,
        route(doormanRoute.open.bind(doormanRoute))
    );

    app.post('/api/login',
        validate(loginRequest.validate.bind(loginRequest)),
        route(loginRoute.login.bind(loginRoute))
    );
    app.post('/api/login-facebook',
        route(loginRoute.loginWithFacebook.bind(loginRoute))
    );
    app.post('/api/logout',
        requireLoggedIn,
        route(loginRoute.logout.bind(loginRoute))
    );

    app.post('/api/register',
        validate(registerRequest.validate.bind(registerRequest)),
        route(registerRoute.register.bind(registerRoute))
    );

    app.post('/api/register/confirm/:code',
        route(registerRoute.registerConfirm.bind(registerRoute))
    );

    app.post('/api/passwordreset',
        route(passwordResetRoute.sendResetEmail.bind(passwordResetRoute))
    );

    app.post('/api/passwordreset/:token',
        validate(passwordResetRequest.validate.bind(passwordResetRequest)),
        route(passwordResetRoute.reset.bind(passwordResetRoute))
    );

    app.get('/api/profile',
        requireLoggedIn,
        route(profileRoute.getProfile.bind(profileRoute))
    );
    app.post('/api/profile/accept-terms',
        requireLoggedIn,
        route(profileRoute.acceptTerms.bind(profileRoute))
    );

    app.post('/api/checkin',
        requireLoggedIn,
        route(checkInRoute.checkIn.bind(checkInRoute))
    );
    
    app.get('/api/checkin',
        requireLoggedIn,
        route(checkInRoute.getCheckIn.bind(checkInRoute))
    );

    app.get('/api/checkins',
        requireLoggedIn,
        route(checkInRoute.getCheckIns.bind(checkInRoute))
    );
        
    app.get('/api/checkedin',
        requireLoggedIn,
        route(checkInRoute.getLastCheckIns.bind(checkInRoute))
    );

    app.get('/api/grades',
        route(gradeRoute.getAll.bind(gradeRoute))
    );

    app.get('/api/cup',
        route(cupRoute.getAll.bind(cupRoute))
    );
    app.get('/api/cup/current',
        route(cupRoute.getCurrentCup.bind(cupRoute))
    );
    app.get('/api/cup/:cupId',
        route(cupRoute.getCup.bind(cupRoute))
    );

    app.post('/api/cup-tick',
        requireLoggedIn,
        validate(createCupTickRequest.validate.bind(createCupTickRequest)),
        route(cupTickRoute.create.bind(cupTickRoute))
    );
    app.delete('/api/cup-tick/:tickId',
        requireLoggedIn,
        validate(destroyCupTickRequest.validate.bind(destroyCupTickRequest)),
        route(cupTickRoute.destroy.bind(cupTickRoute))
    );

    app.get('/api/my-reservations',
        requireLoggedIn,
        route(reservationRoute.getForUser.bind(reservationRoute))
    )
    app.get('/api/reservations',
        requireLoggedIn,
        route(reservationRoute.getForRange.bind(reservationRoute))
    )
    app.post('/api/reservations',
        requireLoggedIn,
        validate(reservationRequest.validate.bind(reservationRequest)),
        route(reservationRoute.create.bind(reservationRoute))
    )
    app.delete('/api/reservations/:id',
        requireLoggedIn,
        validate(reservationRequest.validateDeleteRequest.bind(reservationRequest)),
        route(reservationRoute.remove.bind(reservationRoute))
    )

    let admin = express.Router();
    admin.use(requireLoggedIn);
    admin.use(requireAdmin);

    admin.get('/reservations\.csv',
        route(reservationRoute.adminDump.bind(reservationRoute))
    )

    admin.get('/doorlog',
        route(doormanRoute.getDoorlogEntries.bind(doormanRoute))
    );

    admin.post('/cup',
        route(cupRoute.create.bind(cupRoute))
    );
    admin.patch('/cup/:cupId',
        route(cupRoute.update.bind(cupRoute))
    );
    admin.delete('/cup/:cupId',
        route(cupRoute.destroy.bind(cupRoute))
    );

    admin.post('/cup-round',
        validate(createCupRoundRequest.validate.bind(createCupRoundRequest)),
        route(cupRoundRoute.create.bind(cupRoundRoute))
    );
    admin.patch('/cup-round/:roundId',
        validate(updateCupRoundRequest.validate.bind(updateCupRoundRequest)),
        route(cupRoundRoute.update.bind(cupRoundRoute))
    );
    admin.delete('/cup-round/:roundId', route(cupRoundRoute.destroy.bind(cupRoundRoute)));

    admin.post('/cup-route',
        validate(createCupRouteRequest.validate.bind(createCupRouteRequest)),
        route(cupRouteRoute.create.bind(cupRouteRoute))
    );
    admin.patch('/cup-route/:routeId',
        validate(updateCupRouteRequest.validate.bind(updateCupRouteRequest)),
        route(cupRouteRoute.update.bind(cupRouteRoute))
    );
    admin.delete('/cup-route/:routeId', route(cupRouteRoute.destroy.bind(cupRouteRoute)));

    app.use('/api/admin', admin);
};