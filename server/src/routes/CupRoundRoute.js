class CupRoundRoute{
    /**
     * @param {CupRoundRepo} cupRoundRepo
     */
    constructor(cupRoundRepo){
        this._cupRoundRepo = cupRoundRepo;
    }

    async create(req, res){
        let id = await this._cupRoundRepo.create(req.body);
        let cupRound = await this._cupRoundRepo.getById(id);

        res.json(cupRound);
    }

    async update(req, res){
        await this._cupRoundRepo.update(req.body, req.params.roundId);
        let cupRound = await this._cupRoundRepo.getById(req.params.roundId);

        res.json(cupRound);
    }

    async destroy(req, res){
        await this._cupRoundRepo.destroy(req.params.roundId);

        res.send(null);
    }
}

module.exports = CupRoundRoute;