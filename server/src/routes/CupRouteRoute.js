class CupRouteRoute{
    /**
     * @param {CupRouteRepo} cupRouteRepo
     */
    constructor(cupRouteRepo){
        this._cupRouteRepo = cupRouteRepo;
    }

    async create(req, res){
        let id = await this._cupRouteRepo.create(req.body);
        let cupRoute = await this._cupRouteRepo.getById(id);

        res.json(cupRoute);
    }

    async update(req, res){
        await this._cupRouteRepo.update(req.body, req.params.routeId);
        let cupRoute = await this._cupRouteRepo.getById(req.params.routeId);

        res.json(cupRoute);
    }

    async destroy(req, res){
        await this._cupRouteRepo.destroy(req.params.routeId);

        res.send(null);
    }
}

module.exports = CupRouteRoute;