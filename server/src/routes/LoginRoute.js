const mustache = require('mustache');

class LoginRoute{
    /**
     * @param {UserRepo} userRepo
     * @param {Emailer} emailer
     * @param {Facebook} facebook
     * @param {FacebookUserDataValidator} facebookUserDataValidator
     */
    constructor(userRepo, emailer, facebook, facebookUserDataValidator){
        this._userRepo = userRepo;
        this._emailer = emailer;
        this._facebook = facebook;
        this._facebookUserDataValidator = facebookUserDataValidator
    }

    async login(req, res){
        let isValidCredentials = await this._userRepo.verifyCredentials(req.body.email, req.body.password);

        if(!isValidCredentials){
            let err = new Error('Ugyldig epost/passord');
            err.status = 400;
            throw err;
        }

        let user = await this._userRepo.getByEmail(req.body.email);

        let remember = req.body.remember;

        this._setLoggedIn(req, user, remember);

        res.status(200).send(null);
    }

    async loginWithFacebook(req, res){
        let fbResponse = req.body;
        let remember = req.body.remember;
        let accessToken = fbResponse.accessToken;

        if(!accessToken){
            let err = new Error('Ugyldig facebook login');
            err.status = 400;
            throw err;
        }

        let data = await this._facebook.getUser(accessToken);
        await this._facebookUserDataValidator.validate(data);

        let user = await this._userRepo.getByEmailOrNull(data.email);

        if(user !== null && !user.confirmed){
            // Facebook login means the email is confirmed
            await this._userRepo.setConfirmed(user.id);
        }

        if(user === null){
            user = await this._userRepo.createUserWithRandomPassword(data);
            await this._userRepo.setConfirmed(user.id);

            let html = mustache.render(`
              <p>Ny bruker registrert via facebook</p>
              <ul>
                <li>Id: {{ user.id }}</li>
                <li>Email: {{ user.email }}</li>
                <li>Firstname: {{ user.firstname }}</li>
                <li>Lastname: {{ user.lastname }}</li>
                <li>Address: {{ user.address }}</li>
                <li>Tlf: {{ user.tlf }}</li>
              </ul>
            `, {user});

            await this._emailer.send('post@buldrekontoret.com', 'Ny bruker registrert via facebook', html);
        }

        this._setLoggedIn(req, user, remember);

        res.status(200).send(null);
    }

    _setLoggedIn(req, user, remember){
        req.session.userId = user.id;

        if(remember){
            req.session.cookie.maxAge = 1000*60*60*24*30;
        }
    }

    async logout(req, res){
        req.session.destroy();
        res.status(200).send(null);
    }
}

module.exports = LoginRoute;