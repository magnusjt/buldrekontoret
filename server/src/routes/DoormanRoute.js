const moment = require('moment');

class DoormanRoute{
    /**
     * @param {Doorman} doorman
     * @param {DoorlogRepo} doorlogRepo
     */
    constructor(doorman, doorlogRepo){
        this._doorman = doorman;
        this._doorlogRepo = doorlogRepo;
    }

    async open(req, res){
        let now = moment();
        let lastOpenedAt = await this._doorlogRepo.lastOpenedAt();

        if(lastOpenedAt !== null){
            let secondsSinceLastOpened = now.diff(lastOpenedAt, 'seconds');
            if(secondsSinceLastOpened < 15){
                let err = new Error('Døra er litt opptatt. Prøv igjen om 15 sekunder');
                err.status = 400;
                throw err;
            }
        }

        await this._doorlogRepo.logDoorOpened(req.user.id);
        await this._doorman.openDoor();
        res.status(200).send(null);
    }

    async getDoorlogEntries(req, res){
        let entries = await this._doorlogRepo.getDoorlogEntries();

        res.status(200).send(entries);
    }
}

module.exports = DoormanRoute;