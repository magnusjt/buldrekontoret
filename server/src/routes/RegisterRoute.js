const mustache = require('mustache');

class RegisterRoute{
    /**
     * @param {UserRepo} userRepo
     * @param {Emailer} emailer
     */
    constructor(userRepo, emailer){
        this._userRepo = userRepo;
        this._emailer = emailer;
    }

    async register(req, res){
        let user = await this._userRepo.create(req.body, req.body.password);

        let confirmationLink = 'http://app.buldrekontoret.com/register/confirm/' + encodeURIComponent(user.confirmationCode);

        let html = mustache.render(`
        <p>
              Følg linken nedenfor for å bekrefte registrering:
            </p>
            <a href="{{confirmationLink}}">{{confirmationLink}}</a>
        `, {confirmationLink});

        await this._emailer.send(req.body.email, 'Bekreft registrering', html);

        res.status(200).send(null);
    }

    async registerConfirm(req, res){
        let code = req.params.code;

        let user = await this._userRepo.getByConfirmationCode(code);

        if(user.confirmed){
            let err = new Error('Bruker er allerede bekreftet');
            err.status = 400;
            throw err;
        }

        await this._userRepo.setConfirmed(user.id);

        let html = mustache.render(`
          <p>Ny bruker registrert</p>
          <ul>
            <li>Id: {{ user.id }}</li>
            <li>Email: {{ user.email }}</li>
            <li>Firstname: {{ user.firstname }}</li>
            <li>Lastname: {{ user.lastname }}</li>
            <li>Address: {{ user.address }}</li>
            <li>Tlf: {{ user.tlf }}</li>
          </ul>
        `, {user});

        await this._emailer.send('post@buldrekontoret.com', 'Ny bruker registrert', html);

        res.status(200).send(null);
    }
}

module.exports = RegisterRoute;