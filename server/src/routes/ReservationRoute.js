class ReservationRoute{
    /**
     * @param {ReservationRepo} reservationRepo
     */
    constructor(reservationRepo){
        this._reservationRepo = reservationRepo;
    }

    async getForRange(req, res){
        const reservations = await this._reservationRepo.getForRange(new Date(req.query.start), new Date(req.query.end));
        res.json({reservations});
    }

    async getForUser(req, res){
        const reservations = await this._reservationRepo.getByUserFrom(req.user.id, new Date(req.query.start));
        res.json({reservations});
    }

    async create(req, res){
        const data = {
            startsAt: new Date(req.body.startsAt),
            endsAt: new Date(req.body.endsAt),
            userId: req.user.id,
        }

        const id = await this._reservationRepo.create(data)
        const reservation = await this._reservationRepo.getById(id)

        res.json(reservation)
    }

    async remove(req, res){
        await this._reservationRepo.remove(req.params.id, req.user.id)

        res.json({})
    }

    async adminDump(req, res){
        // const rows = await this._reservationRepo.adminDump(req.query.includeSensitiveData === 'true')
        // Don't expose this data anymore.
        const rows = []

        res.setHeader('Content-disposition', 'attachment; filename=reservations.csv');
        res.setHeader('Content-type', 'text/csv; charset=utf-8');

        const data = rows.map(row => {
            return [
                row.startsAt.toISOString(),
                row.endsAt.toISOString(),
                row.firstname + ' ' + row.lastname,
                row.email || '',
                row.address || '',
                row.tlf || '',
            ]
                .map(col => col.replace(/,/g, ''))
                .join(',')
        }).join('\r\n')

        res.send(data)
    }
}

module.exports = ReservationRoute;