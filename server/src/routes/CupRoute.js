class CupRoute{
    /**
     * @param {CupRepo} cupRepo
     * @param {CupRoundRepo} cupRoundRepo
     * @param {CupRouteRepo} cupRouteRepo
     * @param {CupTickRepo} cupTickRepo
     */
    constructor(cupRepo, cupRoundRepo, cupRouteRepo, cupTickRepo){
        this._cupRepo = cupRepo;
        this._cupRoundRepo = cupRoundRepo;
        this._cupRouteRepo = cupRouteRepo;
        this._cupTickRepo = cupTickRepo;
    }

    async getAll(req, res){
        const cups = await this._cupRepo.getAll();

        res.json(cups);
    }

    async getCup(req, res){
        const cupId = req.params.cupId;

        const data = await this._getCupData(cupId);

        res.json(data);
    }

    async getCurrentCup(req, res){
        const cup = await this._cupRepo.getCurrent();

        const data = await this._getCupData(cup.id);

        res.json(data);
    }

    async _getCupData(cupId){
        const cup = await this._cupRepo.getById(cupId);
        const rounds = await this._cupRoundRepo.getByCupId(cupId);
        const routes = await this._cupRouteRepo.getByRoundIds(rounds.map(r => r.id));
        const ticks = await this._cupTickRepo.getByRouteIds(routes.map(r => r.id));

        return {cup, rounds, routes, ticks};
    }

    async create(req, res){
        let id = await this._cupRepo.create(req.body);
        let cup = await this._cupRepo.getById(id);

        res.json(cup);
    }

    async update(req, res){
        await this._cupRepo.update(req.body, req.params.cupId);
        let cup = await this._cupRepo.getById(req.params.cupId);

        res.json(cup);
    }

    async destroy(req, res){
        await this._cupRepo.destroy(req.params.cupId);

        res.send(null);
    }
}

module.exports = CupRoute;