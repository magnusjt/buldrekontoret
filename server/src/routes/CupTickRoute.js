class CupTickRoute{
    /**
     * @param {CupTickRepo} cupTickRepo
     */
    constructor(cupTickRepo){
        this._cupTickRepo = cupTickRepo;
    }

    async create(req, res){
        let data = {
            userId: req.user.id,
            routeId: req.body.routeId
        };
        let id = await this._cupTickRepo.create(data);
        let tick = await this._cupTickRepo.getById(id);

        res.json(tick);
    }

    async destroy(req, res){
        await this._cupTickRepo.destroy(req.user.id, req.params.tickId);

        res.send(null);
    }
}

module.exports = CupTickRoute;