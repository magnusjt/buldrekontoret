const moment = require('moment');

class CheckInRoute{
    /**
     * @param {CheckInRepo} checkInRepo
     */
    constructor(checkInRepo){
        this._repo = checkInRepo;
    }

    async checkIn(req, res){
        let userId = req.user.id;
        let lastCheckIn = await this._repo.getLastCheckIn(userId);

        if(lastCheckIn && !lastCheckIn.cooledDown()){
            let err = new Error('No cheating, du sjekket nettop inn!');
            err.status = 400;
            throw err;
        }

        let checkInId = await this._repo.checkIn(userId);
        let checkIn = await this._repo.getById(checkInId);
        res.status(200).send(checkIn);
    }

    async getCheckIn(req, res){
        let userId = req.user.id;
        let lastCheckIn = await this._repo.getLastCheckIn(userId);

        res.status(200).send(lastCheckIn);
    }

    async getCheckIns(req, res){
        let checkIns = await this._repo.getCheckIns();
        res.status(200).send(checkIns);
    }
    
    async getLastCheckIns(req, res){
        let cutoff = moment().subtract(12, "h");
        let checkedIn = await this._repo.getLastCheckIns(cutoff.toDate()); //TODO: config this cutoff..
        res.status(200).send(checkedIn);
    }
}

module.exports = CheckInRoute;