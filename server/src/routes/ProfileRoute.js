const _ = require('lodash');

const getProfile = (user) => {
    return {
        user: _.pick(user, [
            'id',
            'email',
            'firstname',
            'lastname',
            'address',
            'tlf',
            'isAdmin',
            'hasAcceptedTerms',
            'createdAt',
            'updatedAt'
        ])
    }
}

class ProfileRoute{
    /**
     * @param {UserRepo} userRepo
     */
    constructor(userRepo){
        this._userRepo = userRepo;
    }

    async getProfile(req, res){
        const profile = getProfile(req.user)
        res.json(profile);
    }

    async acceptTerms(req, res){
        await this._userRepo.acceptTerms(req.user.id)
        const user = await this._userRepo.getById(req.user.id)
        const profile = getProfile(user)
        res.json(profile)
    }
}

module.exports = ProfileRoute;