const mustache = require('mustache');

class PasswordResetRoute{
    /**
     * @param {UserRepo} userRepo
     * @param {PasswordResetRepo} passwordResetRepo
     * @param {Emailer} emailer
     */
    constructor(userRepo, passwordResetRepo, emailer){
        this._userRepo = userRepo;
        this._passwordResetRepo = passwordResetRepo;
        this._emailer = emailer;
    }

    async sendResetEmail(req, res){
        let user = await this._userRepo.getByEmail(req.body.email);
        if(!user.confirmed){
            let err = new Error('Bruker er ikke bekreftet');
            err.status = 400;
            throw err;
        }

        let pwReset = await this._passwordResetRepo.create(user.id);

        let resetLink = 'http://app.buldrekontoret.com/passwordreset/' + encodeURIComponent(pwReset.token);

        let html = mustache.render(`
            <p>
                Følg linken nedenfor for å endre passord:
            </p>
            <a href="{{resetLink}}">{{resetLink}}</a>
        `, {resetLink});

        await this._emailer.send(req.body.email, 'Tilbakestilling av passord', html);

        res.status(200).send(null);
    }

    async reset(req, res){
        let token = req.params.token;
        let pwReset = await this._passwordResetRepo.get(token);
        await this._passwordResetRepo.remove(token);

        await this._userRepo.changePassword(pwReset.userId, req.body.password);

        res.status(200).send(null);
    }
}

module.exports = PasswordResetRoute;