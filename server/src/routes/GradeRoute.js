class GradeRoute{
    /**
     * @param {GradeRepo} gradeRepo
     */
    constructor(gradeRepo){
        this._gradeRepo = gradeRepo;
    }

    async getAll(req, res){
        const grades = await this._gradeRepo.getAll();

        res.json(grades);
    }
}

module.exports = GradeRoute;