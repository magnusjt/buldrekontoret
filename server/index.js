const path = require('path');
const createContainer = require('./src/createContainer');

require('dotenv').config({
    silent: true,
    path: path.resolve(__dirname, './.env')
});

process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

const ioc = createContainer();
const server = ioc['HttpServer'];
/** @var {WebSocketServer} wsServer */
const wsServer = ioc['WebSocketServer'];
const port = ioc['CONFIG']['PORT'];

process.on('uncaughtException', err => {
    console.error(err);
    process.exit(1);
});

process.on('unhandledRejection', err => {
    console.error(err);
});

server.listen(port, () => {
    console.log('Server listening on port ' + port);
});

wsServer.listen()