# This file describes how the digitalocean server has been installed

We start by adding some super basic things such as users, git repos, and ansible.
Then we use ansible to provisioning as much as possible.

## Add group for devteam
````
addgroup devteam
````

## Add initial user(s)
````
adduser magnusjt
usermod -a -G sudo,devteam magnusjt
su - magnusjt
cd ~
mkdir .ssh
chmod 700 .ssh
cd .ssh
touch authorized_keys
chmod 600 authorized_keys
````

Paste in public key now

If the server is 100% fresh, the key should be uploaded to root by digitalocean.
If so, copy the key from root ~/.ssh

## Install basic packages and stuff
````
sudo su
apt-get install software-properties-common
apt-get install git
apt-add-repository ppa:ansible/ansible
apt-get update
apt-get upgrade
apt-get install ansible
````

## Create the remote git repo
````
sudo su
cd /var
mkdir gitrepos

# Set owner and group of directory
chown magnusjt.devteam gitrepos

# Make sure files in the directory has group permission set (that's the 2 in 2755 when chmod'ing directories)
# Also need a umask of 0002 for this, but that's default so we're good
chmod 2755 gitrepos
````

## Clone the repo
````
git clone https://gitlab.com/magnusjt/buldrekontoret.git /var/gitrepos/bkinstall
````

## Install everything else using ansible

Create file /var/gitrepos/bkinstall/provision/group_vars/prod/main.yml with secrets (mysql user/pw)
The secrets file should have permissions 660 so it cannot be read by others.

````
cd /var/gitrepos/bk/provision
ansible-playbook install.yml --i=inventory --ask-become-pass --connection=local
````