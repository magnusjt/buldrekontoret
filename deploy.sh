#!/usr/bin/env bash

####
# Move this file to /var/gitrepos/deploy.sh
# (chmod 770 deploy.sh)
####

####
# Ensure we can run sudo without pw
# sudo su
# echo "%devteam ALL=(ALL) NOPASSWD: /var/gitrepos/deploy.sh" > /etc/sudoers.d/devteam
# chmod 440 /etc/sudoers.d/devteam
#
# (Or use: visudo -f /etc/sudoers.d/devteam)
####

SOURCE_REPO="https://gitlab.com/magnusjt/buldrekontoret.git"
FINAL_DEPLOY_DIR="/var/gitrepos/bk"
ARTIFACTS_DIR="/var/gitrepos/artifacts"
ENV_FILE="/var/gitrepos/.env"
DATE=`date '+%Y%m%d_%H%M%S'`
REPO_DIR="${ARTIFACTS_DIR}/bk_${DATE}"

# Ensure artifact dir exists
mkdir -p "${ARTIFACTS_DIR}"

echo "Cloning repository"
git clone "${SOURCE_REPO}" "${REPO_DIR}"

echo "Check out deploy branch"
cd "${REPO_DIR}" && git checkout deploy

echo "Installing server node modules"
cd "${REPO_DIR}/server" && npm install

echo "Installing client node modules"
cd "${REPO_DIR}/client" && npm install

echo "Building client"
cd "${REPO_DIR}/client" && npm run build

echo "Copy .env file"
sudo cp -a "${ENV_FILE}" "${REPO_DIR}/server"

# Set permission and owner again in case it wasn't properly set
sudo chmod 660 "${REPO_DIR}/server/.env"
sudo chown doorman:devteam "${REPO_DIR}/server/.env"

ln -sfn "${REPO_DIR}" "${FINAL_DEPLOY_DIR}"
sudo supervisorctl restart doorman