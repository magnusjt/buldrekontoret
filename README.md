# Install
- Read INSTALLATION_REMOTE.md for installation of digitalocean server (Ubuntu 14)
- Read raspi/install.sh for installation of the raspberry pi

# Deploy
- Read DEPLOY_REMOTE.md

# Local/dev
- Install nodejs. Use version 8.
  Similarly, we need a new browser to get async/await there. Chrome is nice.
- Install node modules for both server and client (`npm install` in client and server folder)
- Start webpack dev server for react by running `npm start` within the client folder.
  The client uses the create-react-app project, so all the fancy-pants webpack stuff is hidden.
- Start nodejs backend server by running `npm start` within the server folder
  NB: You need to copy the .env-sample and rename to .env if you want to set any environment variables such as database credentials etc.
- If you want, you can also install a virtual server similar to production by running `vagrant up` within the root folder.
  For this to work, you also need to have installed virtualbox and vagrant.
  Most likely you only want this because it has a mysql server you can use for testing.

The webpack dev server is setup to proxy to our nodejs server on port 3001 (see client/package.json). Make sure the nodejs server runs on that port.
You can set this in the .env file.

# Overview
We have the following components

- digitalocean server, running two nodejs apps
-- server/index.js - Serving a single page reactjs app with a nodejs backend api
-- server/proxy.js - Proxy requests directly to the truportal system. Required logged in and admin (same session used in both apps).

- raspberry pi
-- connected to a router at buldrekontoret
-- automatically sets up ssh remote port forwards to digitalocean server

- truportal system
-- connected to a router at buldrekontoret

# SSH Tunnels
The raspberry is set up with two remote tunnels. One for SSH and one for truportal.

1. Port 2222 on the digitalocean server forwards to port 22 on the raspberry
2. Port 8080 on the digitalocean server forwards to 192.168.1.102:443 from the raspberry (i.e. to the truportal)

192.168.1.102 is the address of the truportal.
The router is set up to permanently assign this IP to the MAC-address of the truportal (DHCP).

# How to access the router, truportal, or anything else on the buldrekontoret network:

1. Proxy through the digitalocean server, then connect to the raspberry on 127.0.0.1:2222 (using the previously set tunnel) on the remote
2. Set up a socks proxy.

You can do this from e.g. windows ubuntu bash (windows subsystem for linux) with one command:

ssh -f -N -D 8888 -p 2222 -o ProxyCommand="ssh -W %h:%p -i /mnt/c/Users/Jeffs/.ssh/id_rsa magnusjt@[remote_ip]" pi@127.0.0.1

The socks proxy is now set up on your home computer on localhost:8888.
Configure the browser to use this proxy, and you can browse like you're at buldrekontoret.

Alternatively, if you just want an easy forward to the truportal:

ssh -N -L 8888:127.0.0.1:8080 -i /mnt/c/Users/Jeffs/.ssh/id_rsa magnusjt@[remote_ip]

Now you can just go to https://localhost:8888 in your browser to get to the truportal (no socks proxy needed).
This uses the other port forward on the remote server.