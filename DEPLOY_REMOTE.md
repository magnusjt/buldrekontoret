# This file describes how to deploy new code the digitalocean server

## Push to server
Push to gitrepo from local using `git push origin deploy`

## Deploy new code
- Go to /var/gitrepos
- Ensure .env file exists (doorman:devteam, permission: 660)
- Run ./deploy.sh

## Run provisioning (If needed. Probably only the first time)
If any changes to installed software and such, run ansible provisioning again.

## Migrate db (If needed. Only if changes to database schema)
Go to /var/gitrepos/bk/server/migrations
Run db tasks if needed