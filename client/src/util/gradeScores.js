export default {
    'green':  1,
    'blue':   10,
    'yellow': 100,
    'red':    1000,
    'black':  5000,
    'white':  25000,
    'silver': 125000
};