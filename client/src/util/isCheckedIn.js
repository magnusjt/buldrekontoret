import moment from "moment";

export const isCheckedIn = (checkInStatus, time) => {
    if(!checkInStatus || !checkInStatus.expiresAt){
        return false;
    }

    const expiration = moment(checkInStatus.expiresAt);
    return moment(time).isBefore(expiration);
}