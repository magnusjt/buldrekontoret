export type User = {
    id: number
    email: string
    firstname: string
    lastname: string
    address: string
    tlf: string
    isAdmin: number
    hasAcceptedTerms: number
    createdAt: string
    updatedAt: string
}

export type Profile = {
    user: User
}