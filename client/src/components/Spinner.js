import React, {Component} from 'react';

export default class Spinner extends Component{
    render(){
        return (
            <span className="fa fa-spinner fa-spin"></span>
        );
    }
}