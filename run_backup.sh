#!/bin/bash

# As root!
#
# mkdir /var/bk_backup
# Put this script in: /var/bk_backup
# Add execute bit: chmod +x /var/bk_backup/run_backup.sh
# echo "0 7 * * * root /var/bk_backup/run_backup.sh > /dev/null 2>&1" > /etc/cron.d/bk_backup

DATE=`date '+%Y%m%d_%H%M%S'`
FILEPATH="/var/bk_backup/backup_${DATE}.sql.gz"
mysqldump doorman | gzip > "${FILEPATH}"
chmod 600 "${FILEPATH}"