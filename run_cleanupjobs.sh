#!/bin/bash

# As root!
#
# Put this script in: /etc/cron.hourly
# Rename to bkcleanup to make stupid cron actually use the file
# Add execute bit: chmod +x /etc/cron.hourly/bkcleanup

mysql -e "DELETE FROM users WHERE confirmed=0 AND createdAt<DATE_SUB(NOW(), INTERVAL 12 HOUR)" doorman