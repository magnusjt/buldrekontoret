#!/bin/bash

ANSIBLE_PLAYBOOK="provision/install_local.yml"
ANSIBLE_HOSTS="provision/inventory"
TEMP_INVENTORY="/tmp/ansible_inventory"

sudo su

if ! command -v ansible >/dev/null; then
    apt-get -y install software-properties-common

    apt-add-repository -y ppa:ansible/ansible

    apt-get -y update
    apt-get -y upgrade

    apt-get -y install ansible
fi

export PYTHONUNBUFFERED=1
export ANSIBLE_FORCE_COLOR=true

# Copy inventory to a place that doesn't have mod 0777 forced
cp /vagrant/${ANSIBLE_HOSTS} ${TEMP_INVENTORY} && chmod -x ${TEMP_INVENTORY}

echo "Running Ansible as $USER:"
ansible-playbook /vagrant/${ANSIBLE_PLAYBOOK} --inventory-file=${TEMP_INVENTORY} --connection=local --verbose

rm ${TEMP_INVENTORY}